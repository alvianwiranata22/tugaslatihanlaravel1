@extends('layout.master')

@section('judul')
    Halaman Pendataan
@endsection

@section('isi')
   <h1>Buat Account Baru</h1>
    <br>
    <h3>Sign Up Form</h3>
    <br>
    <form action="/post" method="POST">
        @csrf
        <label>Nama Depan</label><br>
        <input type="text" name="nama_depan"><br>

        <label>Nama Belakang</label><br>
        <input type="text" name="nama_belakang"><br><br>

        <label>Gender</label><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br><br>

        <label>Nationality</label><br>
        <select name="Nationality">
            <option value="Indonesian">Indoensian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label >Language Spoken:</label>
            <br><br>
        <input type="checkbox">Bahasa Indonesian
            <br>
        <input type="checkbox">English
            <br>
        <input type="checkbox">Other
            <br><br>

        <label >Bio:</label>
            <br><br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea>
            <br><br>

        <input type="submit" value="kirim">
        </form>
@endsection
    

