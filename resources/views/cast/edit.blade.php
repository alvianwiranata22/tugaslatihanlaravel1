@extends('layout.master')

@section('judul')
    <h2>Edit cast {{$cast->id}}</h2>
@endsection
    
@section('isi')

<div>
    
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}"  placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="integer" class="form-control" name="umur"  value="{{$cast->umur}}"   placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label >Bio</label>
            <input type="text area" class="form-control" name="bio"  value="{{$cast->bio}}"   placeholder="Masukkan Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

@endsection