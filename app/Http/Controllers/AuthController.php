<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.register');
    }

    public function kirim(Request $request){
        $nama1 = $request->nama_depan;
        $nama2 = $request->nama_belakang;
        
        return view('page.welcome', compact('nama1','nama2'));

    }
}
